import React from 'react';
import { Switch, Route } from 'react-router-dom';
import config from './config.development.json';
import DialogManager from './core/DialogManager';

// Containers
import IndexPage from './containers/IndexPage';
import DialogPage from './containers/DialogPage';

import './App.css';

class App extends React.Component {

	constructor(){
		super();

		this.dm = new DialogManager({
			serverHost: config.SERVER_HOST
		});

	}

	render(){
		return (
			<div className="App">

				<Switch>
					<Route exact path="/dialog" render={(props) => <DialogPage {...props} dialogManager={this.dm} />} />
 					<Route path="/:dialogHash?" render={(props) => <IndexPage  {...props} dialogManager={this.dm} />} />
				</Switch>
				
			</div>
		);
	}

}

export default App;