import React from 'react';
import { shallow } from 'enzyme';
import Button from './index';

test('(components) Button - render', () => {
	var componentReact, componentStatic;
	
	componentReact = shallow(<Button type="primary">Primary Button</Button>);
	componentStatic = <button type="button" className="Button primary">Primary Button</button>;
	expect(componentReact).toContainReact(componentStatic);
});