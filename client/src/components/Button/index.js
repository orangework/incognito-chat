import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

class Button extends React.Component {

	constructor(){
		super();

		// Добавить метод для блокировки кнопки, пока коллбек не сработал
		this.state = {};
	}

	render(){
		let className = `Button ${this.props.type}`;
		if(this.props.className)
			className += ` ${this.props.className}`;

		return(
			<button
				{...this.props}
				className={className}
				type="button"
			>{this.props.children}</button>
		);
	}

}

Button.propTypes = {
	type: PropTypes.string
};

export default Button;