import React from 'react';
import { shallow } from 'enzyme';
import DialogDisplay from './index';

test('(components) DialogDisplay - render', () => {
	var componentReact, componentStatic;
	
	componentReact = shallow(<DialogDisplay />);
	componentStatic = <div className="DialogDisplay" />;
	expect(componentReact).toContainReact(componentStatic);
});