import React from 'react';
import './style.css';

import DialogMessage from '../DialogMessage';


class DialogDisplay extends React.Component {

	constructor(){
		super();
	}

	render(){
		let className = 'DialogDisplay';
		if(this.props.className)
			className += ` ${this.props.className}`;

		let messages = this.props.messages || [];

		return (
			<div className={className}>
				{messages.map(msg => <DialogMessage key={msg.createdAt} {...msg} />)}
			</div>
		)
	}
}

export default DialogDisplay;