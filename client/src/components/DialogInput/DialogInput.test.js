import React from 'react';
import { shallow } from 'enzyme';
import DialogInput, { MIN_ROWS, MAX_ROWS } from './index';

test('(components) DialogInput - render', () => {
	let boundFunction = (function(){}).bind();

	let componentReact = shallow(<DialogInput />);

	// Проверяем создание
	expect(componentReact.exists()).toBe(true);

	// Проверяем ввод
	componentReact.find('textarea').simulate('change', {
		target: { value: 'Hello' }
	});
	expect(componentReact.find('textarea').props().value).toEqual('Hello');
	expect(componentReact.find('textarea').props().rows).toEqual(1);


	// Проверяем увеличение количества строк
	componentReact.find('textarea').simulate('change', {
		target: { value: 'Hello\nWorld' }
	});
	expect(componentReact.find('textarea').props().value).toEqual('Hello\nWorld');
	expect(componentReact.find('textarea').props().rows).toEqual(2);

	// Проверяем блокировку textarea
	componentReact = shallow(<DialogInput disabled />);
	expect(componentReact.find('textarea').props().disabled).toBe(true);
});