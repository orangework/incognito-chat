import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import './style.css';

export const MIN_ROWS = 1, MAX_ROWS = 5;

class DialogInput extends React.Component {

	constructor(){
		super();

		this.state = {
			input: '',
			rows: MIN_ROWS
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleKeyDown = this.handleKeyDown.bind(this);
		this.submit = this.submit.bind(this);

		this.input = React.createRef();
		this.focusInput = this.focusInput.bind(this);
	}

	componentDidMount(){
		window.addEventListener('keydown', this.focusInput);
	}

	render(){

		let disabled = this.props.disabled ? true : false;
		let className = "DialogInput";
		if(disabled)
			className += " disabled";

		return(
			<div className={className}>
				<span>
					<b>Shift</b> + <b>Enter</b> to go to new line
				</span>
				<textarea
					style={{overflow: this.state.rows === MIN_ROWS ? 'hidden' : 'auto'}}
					ref={this.input} rows={this.state.rows}
					onKeyDown={this.handleKeyDown}
					onChange={this.handleChange}
					value={this.state.input}
					disabled={disabled}
				/>
				<Button type="primary" style={{marginTop: '15px'}} onClick={this.submit}>Отправить</Button>
			</div>
		);
	}

}

DialogInput.propTypes = {
	disabled: PropTypes.bool,
	onSubmit: PropTypes.func
};

/**********************
** Методы компонента **
**********************/

DialogInput.prototype.focusInput = function(){
	if(this.input && this.input.current) this.input.current.focus();
}

DialogInput.prototype.submit = function(){
	let inputValue = this.state.input;
	if(inputValue.trim()){
		this.props.onSubmit(this.state.input);
		this.setState({input: ''});
	}
}

DialogInput.prototype.handleChange = function(e){
	let { value } = e.target;
	let rows = value.split("\n").length;

	if(rows > MAX_ROWS) rows = MAX_ROWS;

	this.setState({input: value, rows});
}

DialogInput.prototype.handleKeyDown = function(e){
	let { key, shiftKey } = e;

	if(key === "Enter" && this.props.onSubmit && !shiftKey){
		e.preventDefault();
		this.submit();
	};

	if(key === "Enter" && shiftKey) return;
}

export default DialogInput;