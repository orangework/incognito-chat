import React from 'react';
import { shallow } from 'enzyme';
import moment from 'moment';
import DialogMessage from './index';

test('(components) DialogMessage - render', () => {
	var componentReact, componentStatic, currentDate = moment(new Date());
	var currentDateString = String(currentDate);

	/* !!! Выключаем в рамках теста жалобы на неправильный формат даты !!! */
	let _consoleWarn = console.warn;
	console.warn = null;
	
	
	// Тест входящих
	componentReact = shallow(<DialogMessage text={"A\nB"} type="incoming" createdAt={currentDateString} />);
	componentStatic = (
		<div className="DialogMessageContainer incoming">
			<div className="DialogMessage incoming" key={currentDateString}>
				<div className="DialogMeta">
					<span className="DialogMessageAuthor" key="xyz">someone</span>
					<span className="DialogMessageCreatedAt" key="zyx">{currentDate.format('HH:MM')}</span>
				</div>
				<p><span>A</span><br/><span>B</span></p>
			</div>
		</div>
	);
	expect(componentReact).toContainReact(componentStatic);

	// Тест исходящих
	componentReact = shallow(<DialogMessage text={"A\nB"} type="outgoing" createdAt={currentDateString} />);
	componentStatic = (
		<div className="DialogMessageContainer outgoing">
			<div className="DialogMessage outgoing" key={currentDateString}>
				<div className="DialogMeta">
					<span className="DialogMessageCreatedAt" key="zyx">{currentDate.format('HH:MM')}</span>
					<span className="DialogMessageAuthor" key="xyz">you</span>
				</div>
				<p><span>A</span><br/><span>B</span></p>
			</div>
		</div>
	);
	expect(componentReact).toContainReact(componentStatic);


	/* !!! Возвращаем console.warn !!! */
	console.warn = _consoleWarn;
});