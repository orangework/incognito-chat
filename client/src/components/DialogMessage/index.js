import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import './style.css';

class DialogMessage extends React.Component {

	render(){
		let text = this.props.text || '';

		let formattedText = this.formatText(text);
		let dialogMeta = this.renderMeta(this.props);

		let containerClassName = 'DialogMessageContainer';
		let messageClassName = 'DialogMessage';
		if(this.props.type){
			containerClassName += ` ${this.props.type}`;
			messageClassName += ` ${this.props.type}`;
		};

		return (
			<div className={containerClassName}>
				<div className={messageClassName} key={this.props.createdAt}>
					<div className="DialogMeta">
						{dialogMeta}
					</div>
					<p>{formattedText}</p>
				</div>
			</div>
		);
	}

}

DialogMessage.propTypes = {
	type: PropTypes.oneOf(['incoming', 'outgoing']),
	createdAt: PropTypes.string,
	text: PropTypes.string
};

/**********************
** Методы компонента **
**********************/

// Форматирование текста. Обработка переноса строки.
DialogMessage.prototype.formatText = function(text){
	let lines = typeof(text) === "string" ? text.split("\n") : text;
	let formattedText = [];

	for(let i = 0; i != lines.length; i++){
		if(i !== 0) formattedText.push(<br key={i} />);
		formattedText.push(<span key={i}>{lines[i]}</span>);
	};

	return formattedText;
}

// "Отрисовка" meta-данных сообщения
DialogMessage.prototype.renderMeta = function(props){
	let author;
	let date = moment(this.props.createdAt).format("HH:MM");

	if(props.type === "incoming") author = "someone";
	if(props.type === "outgoing") author = "you";

	let dialogMeta = [
		<span className="DialogMessageAuthor" key="xyz">{author}</span>,
		<span className="DialogMessageCreatedAt" key="zyx">{date}</span>
	];

	if(author === "you") dialogMeta.reverse();

	return dialogMeta;
}

export default DialogMessage;