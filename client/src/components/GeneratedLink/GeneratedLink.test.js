import React from 'react';
import { shallow } from 'enzyme';
import GeneratedLink from './index';

test('(components) GeneratedLink - render', () => {
	var componentReact, componentStatic;
	
	componentReact = shallow(<GeneratedLink>http://www.domain.com/info</GeneratedLink>);
	componentStatic = (
		<p className="GeneratedLink">
			http://www.domain.com/info
		</p>
	);
	expect(componentReact).toContainReact(componentStatic);

});