import React from 'react';
import './style.css';

class GeneratedLink extends React.Component {

	render(){
		return (
			<p className="GeneratedLink">
				{this.props.children}
			</p>
		);
	}

}

export default GeneratedLink;