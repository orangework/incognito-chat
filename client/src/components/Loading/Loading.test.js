import React from 'react';
import { shallow } from 'enzyme';
import Loading from './index';
import svg from './loading.svg';

test('(components) Loading - render', () => {
	var componentReact, componentStatic;
	
	// Без комментария
	componentReact = shallow(<Loading />);
	componentStatic = <img className="Loading" src={svg} />;
	expect(componentReact).toContainReact(componentStatic);

	// С комментарием
	componentReact = shallow(<Loading comment="wait ..." />);
	componentStatic = (
		<div className="LoadingWrapper">
			<img className="Loading" src={svg} />
			<p className="LoadingComment">wait ...</p>
		</div>
	);
	expect(componentReact).toContainReact(componentStatic);
});