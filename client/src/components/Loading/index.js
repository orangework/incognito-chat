import React from 'react';
import PropTypes from 'prop-types';
import svg from './loading.svg';
import './style.css';

class Loading extends React.Component {

	render(){
		let comment = this.props.comment ? <p className="LoadingComment">{this.props.comment}</p> : null;

		if(comment)
			return (
				<div className="LoadingWrapper">
					<img className="Loading" src={svg} />
					{comment}
				</div>
			);
		else
			return (
				<img className="Loading" src={svg} />
			);
	}

}

Loading.propTypes = {
	comment: PropTypes.string
};

export default Loading;