import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
// Components
import Button from '../../components/Button';
import DialogInput from '../../components/DialogInput';
import DialogDisplay from '../../components/DialogDisplay';
// Styles
import './style.css';

class DialogPage extends React.Component {

	constructor(props){
		super();

		this.state = {
			dialogHash: null,
			messages: [],
			redirect: false,
			ready: false
		}

		this.dm = props.dialogManager;
		this.sendMessage = this.sendMessage.bind(this);
		this.onMessage = this.onMessage.bind(this);
		this.onError = this.onError.bind(this);
		this.onReady = this.onReady.bind(this);
		this.leaveDialog = this.leaveDialog.bind(this);
	}

	async componentDidMount(){
		try{
			let dialogHash;
			let props = this.props;
			if(props.location && props.location.state)
				dialogHash = props.location.state.dialogHash;
			else
				dialogHash = this.dm.activeDialog;

			if(dialogHash)
				this.setState({dialogHash}, () => {
					this.dm.open(this.state.dialogHash);
					this.dm.onMessage = this.onMessage;
					this.dm.onError = this.onError;
					this.dm.onReady = this.onReady;
				});
			else
				this.setState({redirect: true});

			let messages = await this.dm.loadMessages();
			this.setState({messages}, () => window.scrollTo(0, window.outerHeight+9999));
		}
		catch(e){
			console.error(e);
		}
	}

	render(){
		if(this.state.redirect) return <Redirect to={{path: "/", state: {error: "JOIN_ERROR"}}} />

		return (
			<div className="DialogPage">
				<Button type="close" onClick={this.leaveDialog}>leave</Button>
				<DialogDisplay messages={this.state.messages} />
				<DialogInput onSubmit={this.sendMessage} disabled={!this.state.ready} />
			</div>
		);
	}

}

DialogPage.propTypes = {
	dialogManager: PropTypes.object
};

/**********************
** Методы компонента **
**********************/

// Обработчик ошибки
DialogPage.prototype.onError = function(){
	this.setState({redirect: true});
}

// Обработчик готовности
DialogPage.prototype.onReady = function(){
	this.setState({ready: true});
}

// Обработчик сообщения
DialogPage.prototype.onMessage = function(msg){
	let isIncoming = msg.type === 'incoming';
	this.setState({messages: [...this.state.messages, msg]}, () => isIncoming || window.scrollTo(0, window.outerHeight+9999));
}

// Отправить сообщение
DialogPage.prototype.sendMessage = function(text){
	this.dm.sendMessage(text);
}

// Выйти из диалога
DialogPage.prototype.leaveDialog = function(){
	let confirmation = window.confirm("Dialog will be destroyed !");
	if(confirmation){
		this.dm.close();
		this.setState({redirect: true});
	}
}
export default DialogPage;