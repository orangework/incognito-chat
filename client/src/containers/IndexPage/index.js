import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import GeneratedLink from '../../components/GeneratedLink';
import Button from '../../components/Button';
import Loading from '../../components/Loading';
import './style.css';

class IndexPage extends React.Component {

	constructor(props){
		super();

		this.state = {
			link: null,
			redirect: false,
			doRedirect: false
		};

		this.createLink = this.createLink.bind(this);
		this.dm = props.dialogManager;
		this.goDialog = this.goDialog.bind(this);
	}

	componentDidMount(){
		let { dialogHash } = this.props.match.params;
		let origin = window.location.origin;
		if(dialogHash){
			this.setState({link: `${origin}/${dialogHash}`, redirect: true, dialogHash});
			setTimeout(this.goDialog, 5000);
		}else if(this.dm && this.dm.activeDialog){
			dialogHash = this.dm.activeDialog;
			this.setState({link: `${origin}/${dialogHash}`, redirect: true, dialogHash}, this.goDialog);
		}
	}

	render(){
		let joinError = null;
		if(this.props.location.state && this.props.location.state.error)
			joinError = this.props.location.state.error === "JOIN_ERROR" ? <p className="warning">Диалог недоступен!</p> : null;

		let joinLink = (this.state.link && !this.state.redirect) ? <GeneratedLink>{this.state.link}</GeneratedLink> : null;
		
		let button;
		if(this.state.link && !this.state.redirect)
			button = <Button type="secondary" onClick={this.createLink}>revoke link</Button>;
		else if(this.state.link && this.state.redirect)
			button = null;
		else
			button = <Button type="primary" onClick={this.createLink}>start dialog</Button>;

		let loading = (this.state.link && this.state.redirect) ? <Loading comment="dialog preparation in progress" /> : null;

		let redirect = this.state.doRedirect ? <Redirect to={{pathname: '/dialog', state: {dialogHash: this.state.dialogHash}}} /> : null;

		return (
			<div className="IndexPage">
				<div className="form">
					<h1>Incognito Chat</h1>
					<h2>invisible messaging</h2>
					{this.state.link || this.state.redirect ? null : joinError}
					{joinLink}
					{button}
					{redirect}
					{loading}
				</div>
			</div>
		);
	}

}

IndexPage.propTypes = {
	dialogManager: PropTypes.object
};

/**********************
** Методы компонента **
**********************/

// Переход в диалог
IndexPage.prototype.goDialog = function(){
	this.setState({doRedirect: true}, () => {
		this.dm.localDB.clear();
	});	
}

// Сгенерировать ссылку на диалог
IndexPage.prototype.createLink = async function(){
	let dialogHash = await this.dm.new();
	let origin = window.location.origin;
	this.setState({link: `${origin}/${dialogHash}`});
}

export default IndexPage;