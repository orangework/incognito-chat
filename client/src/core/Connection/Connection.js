import socketIOClient from 'socket.io-client';
import { isUri } from 'valid-url';
import isValidDomain from 'is-valid-domain';

class Connection {
	
	constructor(socketHost){
		
		// Проверяем, передан ли socketHost
		if(socketHost === undefined || socketHost === null)
			throw new Error('Required parameter "socketHost" is missing');
		// Если передан, должен быть типа String
		else if(typeof(socketHost) !== 'string')
			throw new Error('Required parameter "socketHost" must be a string');

		let hostname = socketHost.trim();

		// Также socketHost должен быть корректным URI адресом или доменом
		let validHostname = isUri(hostname) || isValidDomain(hostname);
		if(!validHostname)
			throw new Error('"socketHost" must be a valid URI or domain');

		this._socketHost = socketHost;
		this._socket = socketIOClient(this._socketHost, { autoConnect: false });

		// Функции on, emit, connect и disconnect оставляем без изменений
		this.connect = this._socket.connect.bind(this._socket);
		this.disconnect = this._socket.disconnect.bind(this._socket);
		this.on = this._socket.on.bind(this._socket);
		this.emit = this._socket.emit.bind(this._socket);
	}



}


export default Connection;