import Connection from './Connection';

const SOCKET_HOST = "https://domain.com";

describe('(core) Connection:', () => {

	test('constructor', () => {
		let conn;
		
		conn = new Connection(SOCKET_HOST);
		expect(conn).toBeInstanceOf(Connection);

	});

});