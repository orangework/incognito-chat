import axios from 'axios';

import LocalDB from '../LocalDB';
import Connection from '../Connection';
import { OutgoingMessage, IncomingMessage } from '../Message';

class DialogManager {

	constructor(config = null){

		if(config === null)
			throw new Error('Required parameter "config" is missing');

		let { serverHost } = config; // Сделать потом отдельно для сокета socketHost

		if(serverHost === undefined)
			throw new Error('Config must contain "serverHost" property')
		
		this._connection = new Connection(serverHost);
		this.serverHost = serverHost;

		// Слушатель для полученных сообщений
		this._connection.on("message", this.receiveMessage.bind(this));
		// Слушатель ошибок
		this._connection.on("fail", this.onFail.bind(this));

		this._connection.on("connect", () => console.info(`Соединение с "${serverHost}" установлено`));
		this._connection.on("disconnect", () => console.info(`Соединение с "${serverHost}" разорвано`));

		this._connection.on("reconnect", () => this.open());

		this._connection.on("event", this.onEvent.bind(this));
		// this.onEvent

		// Коллбек для получения сообщений
		this.onMessage = config.onMessage || null;
		// Коллбек для обработки ошибки
		this.onError = config.onError || null;

		// Локальная база данных
		this.localDB = new LocalDB();
	}

	async new(){
		try{
			let response = await axios.get(`${this.serverHost}/new`);
			let { dialogHash } = response.data;

			return dialogHash;
		}
		catch(e){

		}
	}

	get uid(){
		let uid = localStorage.getItem("uid");

		if(uid)
			return uid;
		else{
			this.createUID();
			return this.uid;
		}
	}

	set uid(value){
		if(value)
			localStorage.setItem("uid", value);
		else
			localStorage.removeItem("uid");
	}

	get activeDialog(){
		let activeDialog = localStorage.getItem("dialog");
		return activeDialog;
	}

	set activeDialog(value){
		if(value)
			localStorage.setItem("dialog", value);
		else
			localStorage.removeItem("dialog")
	}

	createUID(){
		let uid = parseInt( String(Math.random()).split(".")[1] ); // Генерация случайного большого числа
		localStorage.setItem("uid", uid);
	}

	open(dialogHash = null){
		if(dialogHash && dialogHash != this.activeDialog)
			this.activeDialog = dialogHash;

		this._connection.connect();

		let joinMessage = {dialog: this.activeDialog, uid: this.uid};
		this._connection.emit('join', JSON.stringify(joinMessage));
	}

	// Нужно уведомление одного собеседника, если второй вышел из диалога
	close(){
		this.activeDialog = null;
		this.uid = null;
		this._connection.disconnect();
	}

	sendMessage(text){
		let newMessage = new OutgoingMessage(text);
		newMessage.dialog = this.activeDialog;
		this._connection.emit('message', JSON.stringify(newMessage));

		this.localDB.set(newMessage);

		if(this.onMessage) this.onMessage(newMessage);
	}

	receiveMessage(jsonMessage){;
		let messageObj = typeof(jsonMessage) === "object" ? jsonMessage : JSON.parse(jsonMessage);
		let message = new IncomingMessage(messageObj.text, messageObj);
		message.dialog = this.activeDialog;

		this.localDB.set(message);

		if(this.onMessage) this.onMessage(message);
	}

	async loadMessages(){
		let rawMessages = await this.localDB.getAll();

		return rawMessages.map(msg => {
			switch(msg.type){
				
				case "incoming":
					return new IncomingMessage(msg.text, {createdAt: msg.createdAt});
					break;

				case "outgoing":
					return new OutgoingMessage(msg.text, {createdAt: msg.createdAt});
					break;

			}
		});
	}

	onFail(error){
		console.log(error);
		if(error === 'JOIN_ERROR'){
			this.close();
			if(this.onError) this.onError();
		}
	}

	onEvent(message){
		if(this.onReady) this.onReady(message);
	}
}

export default DialogManager;