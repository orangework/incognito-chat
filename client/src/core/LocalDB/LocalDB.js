import { openDB, deleteDB, wrap, unwrap } from 'idb';

class LocalDB {
	
	constructor(){
		
		if(window.indexedDB){
			this._dbPromise = openDB('main-db', 1, {
				upgrade(db) {
					let store = db.createObjectStore('messages', {
						autoIncrement: true,
						keyPath: "id"
					});
				}
			});

			this._disableIncognitoWarning = false;
		}
		else console.error(`Browser doesn't support indexedDB ! Use:\nChrome > 24+\nOR\nFirefox > 16+\nOR\nSafari > 10+\nOR\nOpera > 10+`);
		
	}

	async get(key){
		return (await this._dbPromise).get('messages', key);
	}

	async set(value){
		return (await this._dbPromise).put('messages', value);
	}

	async delete(key){
		return (await this._dbPromise).delete('messages', key);
	}

	async clear(){
		return (await this._dbPromise).clear('messages');
	}

	async getAll(){
		try{
			return (await this._dbPromise).getAll('messages');
		}
		catch(e){
			switch(e.name){
				case "InvalidStateError":
					if(!this._disableIncognitoWarning) this._disableIncognitoWarning = window.confirm("Messages won't be saved in Incognito Mode");
					break;
			}	
		}
	}

	async getKeys(){
		return (await this._dbPromise).getAllKeys('messages');
	}

}


export default LocalDB;