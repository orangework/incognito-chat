import Message from './Message';

class IncomingMessage extends Message {

	constructor(){
		super(...arguments);

		this.type = "incoming";
	}

}

export default IncomingMessage;