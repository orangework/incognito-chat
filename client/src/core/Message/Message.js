import moment from 'moment';

class Message {

	constructor(text, options = {}){
		this.text = text;
		
		this._createdAt = options.createdAt ? moment(options.createdAt).toString() : moment( new Date() ).toString();
		this.timestamp = moment(this._createdAt).unix();
	}
	
	get createdAt(){
		return moment(this._createdAt);
	}

	encode(){

	}

	decode(){

	}
}



export default Message;


// body: String | Blob - Тело сообщения. Картинка
// createdAt: Date - Дата создания
// receivedAt: Date - Дата получение
// encode(): Function - Кодирование из текста в картинку
// decode(): Function - Декодирование из картинки в текст