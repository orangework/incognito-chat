import Message from './Message';

class OutgoingMessage extends Message {

	constructor(){
		super(...arguments);

		this.type = "outgoing";
	}

}

export default OutgoingMessage;