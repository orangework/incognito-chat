var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var Dialog = require('./src/Dialog');

const cors = require('cors');

var dialogs = {};

app.use(cors());

app.get('/new', (req, res) => {
	let newDialog = new Dialog();
	dialogs[newDialog.hash] = newDialog;

	res.send({dialogHash: newDialog.hash});
});

io.on('connection', (socket) => {

	socket.on('join', (message) => {
		message = JSON.parse(message.trim());

		let dialogHash = message.dialog.trim();
		let dialog = dialogs[dialogHash];

		try{
			dialog.updateMemberSocket({socket, uid: message.uid});

			console.log(`UID: ${message.uid}`);
			console.log(`Joined: ${dialog.members.map(el => el.uid).join(" <-> ")}`);

			if(dialog.ready) dialog.members.forEach(({socket}) => socket.emit('event', 'READY'));
		}
		catch(e){
			console.log(e);
			socket.emit('fail', 'JOIN_ERROR');
			return false;
		}
	})

	socket.on('message', (message) => {
		try{
			message = JSON.parse(message.trim());
			let dialogHash = message.dialog.trim();
			let dialog = dialogs[dialogHash];

			let target = dialog.getAddress(socket);

			target.emit('message', message);
		}
		catch(e){
			console.log(e);
		}
	});
});

// setInterval(() => console.log(dialogs), 5000);

http.listen(8050, () => {
	console.log('listening on *:8050');
});