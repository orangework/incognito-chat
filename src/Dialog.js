const moment = require('moment');
const randomHash = require('./utils/random-hash');

class Dialog {

	constructor(){
		this.createdAt = moment(new Date());
		this.hash = randomHash();
		this.isOpened = false;
		//this.members = new Array(2);
		this.members = [];
		this.ready = false;
	}

	addMember(socketAndUID){
		if(this.members.length < 2){
			this.members.push(socketAndUID);
			if(this.members.length == 2) this.ready = true;
		}
		else
			throw new Error('Dialog can\'t contain more than 2 members');
	}

	getAddress(socket){
		let targetSocket = this.members.find(socketAndUID => socketAndUID.socket.id !== socket.id);
		console.log(`TARGET: ${socket.id}`);
		this.members.forEach(el => console.log(`- ${el.socket.id}`));
		return targetSocket.socket;
	}

	updateMemberSocket(socketAndUID){
		let targetSocket = this.members.find(m => m.uid === socketAndUID.uid);
		console.log(`->`, targetSocket);
		if(targetSocket)
			targetSocket.socket = socketAndUID.socket;
		else
			this.addMember(socketAndUID);

	}

}


module.exports = Dialog;