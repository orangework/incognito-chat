const randomLetters = require('random-letters');

const HASH_LENGTH = 12;

function randomHash(){
	return randomLetters(HASH_LENGTH);
}

module.exports = randomHash;